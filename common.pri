DLL_DIR = $$PWD
LIB_DIR = $$PWD/lib

include($$PWD/config.pri)
contains(ENABLE_QT_SINGLE_APPLICATION, yes): CONFIG += qtsingleapplication-uselib
TEMPLATE += fakelib
QTSINGLEAPPLICATION_LIBNAME = $$qtLibraryTarget(QtSolutions_SingleApplication-head)
TEMPLATE -= fakelib

unix:qtsingleapplication-uselib:!qtsingleapplication-buildlib:QMAKE_RPATHDIR *= $$LIB_DIR
