#-------------------------------------------------
#
# Project created by QtCreator 2012-09-19T09:55:32
#
#-------------------------------------------------
#The project file is locate at build_imageViewer
PROGRAMNAME = ImageViewer
TR_DIR = $${PWD}/language/

TRANSLATIONS = $${TR_DIR}zh_TW/$${PROGRAMNAME}.ts \
                           $${TR_DIR}zh_HK/$${PROGRAMNAME}.ts \
                           $${TR_DIR}jp/$${PROGRAMNAME}.ts \
                           $${TR_DIR}fr/$${PROGRAMNAME}.ts \
                           $${TR_DIR}it/$${PROGRAMNAME}.ts \
                           $${TR_DIR}de/$${PROGRAMNAME}.ts \
                           $${TR_DIR}es/$${PROGRAMNAME}.ts \
                           $${TR_DIR}pl/$${PROGRAMNAME}.ts \
                           $${TR_DIR}cs/$${PROGRAMNAME}.ts \
                           $${TR_DIR}ru/$${PROGRAMNAME}.ts \
                           $${TR_DIR}en_US/$${PROGRAMNAME}.ts \
                           $${TR_DIR}en_GB/$${PROGRAMNAME}.ts
#To add a new translation, add a \ behide the above line.
#and type: $${TR_DIR}$${PROGRAMNAME}_[LanguageCode].ts

#LanguageCode format "language[_script][_country][.codeset][@modifier]" or "C".
#Usually use like this: language[_country], where:
    #language is a lowercase, two-letter, ISO 639 language code,
    #script is a titlecase, four-letter, ISO 15924 script code,
    #country is an uppercase, two- or three-letter, ISO 3166 country code (also "419" as defined by United Nations),
    #and codeset and modifier are ignored.
#more detail please check document about QLocale

QT       += core gui
TEMPLATE = app
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets
    win32: DEFINES += Q_WS_WIN
}

OBJECTS_DIR = $${PWD}/obj/
MOC_DIR = $${PWD}/obj/
RCC_DIR = $${PWD}/obj/

contains(DEFINES, SINGLE_APP) {
        PROGRAMNAME = $${PROGRAMNAME}_single
        OBJECTS_DIR = $${OBJECTS_DIR}single_
        MOC_DIR = $${MOC_DIR}single_
        RCC_DIR = $${RCC_DIR}single_
}

#output dir
DESTDIR = $${PWD}/
CONFIG(debug, debug|release) {
        debug: TARGET = $${PROGRAMNAME}_debug
        OBJECTS_DIR = $${OBJECTS_DIR}debug
        MOC_DIR = $${MOC_DIR}debug
        RCC_DIR = $${RCC_DIR}debug
        win32: CONFIG += console
#contains(DEFINES, SINGLE_APP): LIBS += -lQtSolutions_SingleApplication-headd
} else {
        release: TARGET = $${PROGRAMNAME}
        OBJECTS_DIR = $${OBJECTS_DIR}release
        MOC_DIR = $${MOC_DIR}release
        RCC_DIR = $${RCC_DIR}release
#contains(DEFINES, SINGLE_APP): LIBS += -lQtSolutions_SingleApplication-head
}

SOURCES += $${PWD}/src/main.cpp\
    $${PWD}/src/mainwindow.cpp \
    $${PWD}/src/imagelabel.cpp \
    $${PWD}/src/propertydialog.cpp \
    $${PWD}/src/graphicsview.cpp \
    $${PWD}/src/imginfomodel.cpp \
    $${PWD}/src/core/supext.cpp \
    $${PWD}/src/core/strnatcmp.c \
    $${PWD}/src/core/naturalsort.cpp \
    $${PWD}/src/core/filenamelist.cpp \
    $${PWD}/src/core/filenamegen.cpp \
    $${PWD}/src/core/langadmin.cpp

HEADERS += $${PWD}/src/mainwindow.hpp \
    $${PWD}/src/imagelabel.hpp \
    $${PWD}/src/propertydialog.hpp \
    $${PWD}/src/graphicsview.hpp \
    $${PWD}/src/imginfomodel.hpp \
    $${PWD}/src/core/supext.hpp \
    $${PWD}/src/core/strnatcmp.h \
    $${PWD}/src/core/naturalsort.hpp \
    $${PWD}/src/core/filenamelist.hpp \
    $${PWD}/src/core/filenamegen.hpp \
    $${PWD}/src/core/langadmin.hpp

FORMS   += $${PWD}/src/mainwindow.ui \
    $${PWD}/src/propertydialog.ui

RESOURCES += \
    $${PWD}/rsc.qrc
win32: RC_FILE += $${PWD}/rsc.rc

OTHER_FILES += \
    $${PWD}/todo.txt
