#include "propertydialog.hpp"
#include "ui_propertydialog.h"
#include "imginfomodel.hpp"
#ifndef NO_QDEBUG
#include <QDebug>
#endif

PropertyDialog::PropertyDialog(QWidget *parent) :
    QDialog(parent),
    mp_ui(new Ui::PropertyDialog)
{
    mp_ui->setupUi(this);
}

PropertyDialog::~PropertyDialog()
{
    delete mp_ui;
}

void PropertyDialog::setModel(ImgInfoModel *p_model)
{
    if(!mp_ui->tableView->model()) {
        mp_ui->tableView->setModel(p_model);
        connect(p_model, SIGNAL(fileChanged(QString)), SLOT(resetTitle(QString)));
    }
    resetTitle(p_model->name());
}

void PropertyDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        mp_ui->retranslateUi(this);
        resetTitle(mp_ui->tableView->model()->property("name").toString());
        break;
    default:
        break;
    }
}

void PropertyDialog::resetTitle(const QString &fileName)
{
    setWindowTitle(tr("Properties of %1").arg(fileName));
}
