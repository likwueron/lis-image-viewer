#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QPrinter>

class QMovie;
class QScrollBar;
class ImgInfoModel;
class PropertyDialog;
class SupExt;
class QActionGroup;
class FileNameGen;
class FileNameList;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    enum State {
        NoImg       =   0x000,
        //basic, seperated
        HasImg      =   0x010,
        NormalOnly  =   0x001,
        FtwOnly     =   0x002,
        ZoomOnly    =   0x003,
        AutoOnly    =   0x100,
        //fusion
        Normal      =   0x011,
        FitToWindow =   0x012,
        Zoom        =   0x013,
        Auto        =   0x110,
        AutoN       =   0x111,
        AutoF       =   0x112
    };

    enum ImgListMode {
        Default = 0,
        Add,
        Remove
    };

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void resetTitle(const QString &fileName);
    void openImg(const QString &fileName);

protected:
    void readSettings();
    void writeSettings();
    void closeEvent(QCloseEvent *e);

    void changeEvent(QEvent *e);

    void resizeEvent(QResizeEvent *);

    void wheelEvent(QWheelEvent *e);

    bool isState(State state) const;

    void dragEnterEvent(QDragEnterEvent *event);

    void dropEvent(QDropEvent *event);
    
private slots:
    void on_actionFitToWindow_triggered(bool checked);
    void on_actionNormalSize_triggered();
    void on_actionZoomIn_triggered();
    void on_actionZoomOut_triggered();
    void on_graphicsView_resized();

    void on_actionNextImg_triggered();
    void on_actionPreImg_triggered();

    void on_actionOpen_triggered();
    void on_actionConvert_triggered();
    void on_actionDel_triggered();
    void on_actionDuplicate_triggered();

    void on_actionPrint_triggered();

    void on_actionProperties_triggered();

    void on_menuLang_triggered(QAction *);

    void on_actionThis_triggered();
    void on_actionQt_triggered();

    void on_actionSetting_smartFitToWindow_triggered();

    void on_actRotateLeft_triggered();
    void on_actRotateRight_triggered();

    void on_actSet_resetScale_toggled(bool checked);

    void on_actSet_resetRotate_toggled(bool checked);

    void when_fileNameListUpdated(QString, int);

private:
    void initLang();
    void setupSupExt();
    void setupNameList();

    void updateActions();
    bool updateImgInfo(const QString &fileName);

    void fitToWindow();
    void scaleImage(const qreal &factor);
    void rotateImage(const qreal &degree);
    //program basic
    Ui::MainWindow *mp_ui;
    QString m_title;
    QSize m_normalSize;
    SupExt *mp_supportFormat;
    FileNameGen *mp_fileNameGen;
    State m_state;
    //img info
    ImgInfoModel *mp_model;
    PropertyDialog *mp_propertyDialog;
    //scale
    static const double sm_maxScale;
    static const double sm_minScale;
    //file list
    FileNameList *mp_fileList;
    //print
    QPrinter m_printer;
    //language
    QActionGroup *mp_actLangs;
};

#endif // MAINWINDOW_HPP
