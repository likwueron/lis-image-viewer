#ifndef GRAPHICSVIEW_HPP
#define GRAPHICSVIEW_HPP

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
    Q_PROPERTY(QString img READ imgName WRITE setImgFromName)
    Q_PROPERTY(qreal scaleFactor READ scaleFactor WRITE scaleImg RESET resetScale)
    Q_PROPERTY(qreal rotateDegree READ rotateDegree WRITE rotateImg RESET resetRotate)
public:
    explicit GraphicsView(QWidget *parent = 0);
    
    QString imgName() const;
    void setImgFromName(const QString &imgName);
    QPixmap pixmap() const;
    void removeImg();

    bool isLoadErr() const;
    bool isNull() const;

    QSizeF imgSize() const;

    qreal scaleFactor() const;
    void scaleImg(const qreal &factor);
    void fitIn();
    void fitWidth();
    void fitHeight();
    void resetScale();
    bool isAutoResetScale() const;
    void setAutoResetScale(bool resetScale);

    qreal rotateDegree() const;
    void rotateImg(const qreal &degree);
    void resetRotate();
    bool isAutoResetRotate() const;
    void setAutoResetRotate(bool resetRotate);

    void resetMatrix();
    void autoResetMatrix();
signals:
    void resized();
public slots:
protected:
    void resizeEvent(QResizeEvent *e);
    void wheelEvent(QWheelEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private:
    QString m_imgName;
    QGraphicsPixmapItem* mp_pix;

    qreal m_scaleFactor;
    bool m_resetScale;

    qreal m_rotateDegree;
    bool m_resetRotate;

    int m_lastX;
    int m_lastY;
};

#endif // GRAPHICSVIEW_HPP
