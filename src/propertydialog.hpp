#ifndef PROPERTYDIALOG_HPP
#define PROPERTYDIALOG_HPP

#include <QDialog>
#include <QMap>

namespace Ui {
class PropertyDialog;
}
class ImgInfo;
class ImgInfoModel;

class PropertyDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit PropertyDialog(QWidget *parent = 0);
    ~PropertyDialog();

    void setModel(ImgInfoModel *p_model);
protected:
    void changeEvent(QEvent *e);
private slots:
    void resetTitle(const QString &fileName);
private:
    Ui::PropertyDialog *mp_ui;
};

#endif // PROPERTYDIALOG_HPP
