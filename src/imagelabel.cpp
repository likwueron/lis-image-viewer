/*
 file name: imagelabel.cpp
 author: Li, Kwue-Ron
 e-mail: likwueron@gmail.com
 published under public domain
*/

#include "imagelabel.hpp"
#include <QMovie>
#include <QMessageBox>
#ifndef NO_QDEBUG
#include <QDebug>
#endif

//construct and deconstruct begin
ImageLabel::ImageLabel(QWidget *parent)
    : QLabel(parent), mp_movie(0x0)
{
    //mp_pix = new QPixmap;
}

ImageLabel::ImageLabel(const QString &imgName, const QString &toolTip,
                       QWidget *parent)
    : QLabel(parent), mp_movie(0x0), m_toolTip(toolTip)
{
    setImgFromName(imgName);
    setToolTipIfImgExist();
}

ImageLabel::~ImageLabel()
{
    clear();
}
//construct and deconstruct end

//read and write properties begin
QString ImageLabel::imgName() const
{
    return m_imgName;
}
QPixmap ImageLabel::img() const
{
    if(pixmap()) {
        return *pixmap();
    }
    if(mp_movie) {
        return mp_movie->currentPixmap();
    }
    return QPixmap();
}
void ImageLabel::setImgFromName(const QString &imgName)
{
    if(m_imgName == imgName) {
        return;
    }
    m_imgName = imgName;
    m_loadErr = false;
    QMovie *p_oldMovie = mp_movie;
    //is it animation?
    if(imgName.endsWith(".gif", Qt::CaseInsensitive)) {
        mp_movie = new QMovie(imgName);
        if(!mp_movie->isValid()) {
            m_loadErr = true;
        }
        else {
            //get size
            mp_movie->stop();
            mp_movie->jumpToFrame(0);
            m_imgSize = mp_movie->currentPixmap().size();

            setMovie(mp_movie);
            mp_movie->start();
        }
    }
    else {
        QImage image(imgName);
        if(image.isNull()) {
            m_loadErr = true;
        }
        else {
            setPixmap(QPixmap::fromImage(image));
            m_imgSize = image.size();
        }
    }

    if(m_loadErr) {
        QWidget *p_w = parentWidget() ? parentWidget() : this;
        QMessageBox::information(p_w, p_w->windowTitle(),
                                 tr("Cannot load %1.").arg(imgName));
        mp_movie = p_oldMovie;
    }
}

void ImageLabel::removeImg()
{
    clear();
    setText(" ");
    m_imgName = "";
    adjustSize();
}

bool ImageLabel::isNull() const
{
    if(pixmap()) {
        return pixmap()->isNull();
    }
    if(mp_movie) {
        return !mp_movie->isValid();
    }
    return false;
}

bool ImageLabel::isLoadErr() const
{
    return m_loadErr;
}

QString ImageLabel::errStr() const
{
    return "Not a proper file";
}

QString ImageLabel::toolTip() const
{
    return m_toolTip;
}
void ImageLabel::setToolTip(const QString &toolTip)
{
    if(m_toolTip == toolTip) {
        return;
    }
    m_toolTip = toolTip;
    setToolTipIfImgExist();
}

QSize ImageLabel::imgSize() const
{
    return m_imgSize;
}

void ImageLabel::resizeEvent(QResizeEvent *e)
{
    QLabel::resizeEvent(e);
    emit resized();
}
//read and write properties end

void ImageLabel::clear()
{
    if(mp_movie) {
        delete mp_movie;
        mp_movie = 0x0;
    }
}

void ImageLabel::loadImg()
{
    return;
}

bool ImageLabel::setToolTipIfImgExist()
{//function will be called while any property be setted/changed
    //all data is assumed not setted
    bool imgLost = isNull();
    //tooltip
    bool toolTipIsEmpty = m_toolTip.isEmpty();
    QString toolTipEmptyMsg = tr("Image Label");
    //if Img load success
    if( imgLost ) {
        //set tooltip as text when image loaded failure
        if( toolTipIsEmpty ) {
            this->setText( toolTipEmptyMsg );
        }
        else {
            this->setText(m_toolTip);
        }
        //set error messange as tooltip
        //: %1 means the location of the not exist image
        QLabel::setToolTip( tr("Image: \"%1\" is not exist!").arg(m_imgName) );
    }
    else {
        //qDebug() << "constructing...";
        /*//set tooltip
        if( toolTipIsEmpty ) {
            QLabel::setToolTip( toolTipEmptyMsg );
        }
        else {
            QLabel::setToolTip(m_toolTip);
        }*/
    }
    return !imgLost;
}
