#include "graphicsview.hpp"
#include <QGraphicsTextItem>
#include <QPixmap>
#include <QImage>
#include <QImageReader>
#ifndef NO_QDEBUG
#include <QDebug>
#endif
#include <QScrollBar>
#include <QWheelEvent>
#include <QMouseEvent>

GraphicsView::GraphicsView(QWidget *parent) :
    QGraphicsView(parent), mp_pix(0x0),
    m_scaleFactor(1.0), m_resetScale(true),
    m_rotateDegree(0.0), m_resetRotate(true)
{
    setMouseTracking(true);
}

QString GraphicsView::imgName() const
{
    return m_imgName;
}

void GraphicsView::setImgFromName(const QString &imgName)
{
    if(!scene()) {
        QGraphicsScene *p_scene = new QGraphicsScene;
        setScene(p_scene);
    }
    else {
        scene()->clear();
    }


    QByteArray format = QImageReader::imageFormat(imgName);
    qDebug() << "format" << format;
    QImage img(imgName, format.data());
    if(!img.isNull()) {
        m_imgName = imgName;
        mp_pix = scene()->addPixmap(QPixmap::fromImage(img));
        setSceneRect(scene()->itemsBoundingRect());
        autoResetMatrix();
    }
    else {
        scene()->clear();
        scene()->
                addText(tr("Can not load \"%1\" properly.\nThis file might broken or too large.")
                        .arg(imgName));
        resetMatrix();

        setSceneRect(scene()->itemsBoundingRect());
        m_imgName = "";
    }
    //show();
}

QPixmap GraphicsView::pixmap() const
{
    return mp_pix->pixmap();
}

void GraphicsView::removeImg()
{
    scene()->clear();
    setSceneRect(scene()->itemsBoundingRect());
    m_imgName = "";
    mp_pix = 0x0;
}

bool GraphicsView::isLoadErr() const
{
    return false;
}

bool GraphicsView::isNull() const
{
    return m_imgName.isEmpty();
}

QSizeF GraphicsView::imgSize() const
{
    QTransform matrix;
    QRectF rect = scene()->itemsBoundingRect();
    matrix.rotate(m_rotateDegree);
    rect = matrix.mapRect(rect);
    return rect.size();
}

qreal GraphicsView::scaleFactor() const
{
    return m_scaleFactor;
}

void GraphicsView::scaleImg(const qreal &factor)
{
    QTransform matrix;
    m_scaleFactor *= factor;
    matrix.scale(m_scaleFactor, m_scaleFactor);
    matrix.rotate(m_rotateDegree);
    setTransform(matrix);
}

void GraphicsView::fitIn()
{
    int frameThick = frameWidth() * 2;
    qreal windowW = size().width() - frameThick;
    qreal windowH = size().height() - frameThick;
    QSizeF oriSize = imgSize();

    qreal wRatio = windowW / oriSize.width();
    qreal hRatio = windowH / oriSize.height();
    qreal scaleFactor = qMin(wRatio, hRatio);
    //resize
    m_scaleFactor = 1.0;
    scaleImg(scaleFactor);
}

void GraphicsView::fitWidth()
{
    int frameThick = frameWidth() * 2;
    qreal windowW = size().width() - frameThick;
    qreal oriW = imgSize().width();
    qreal scaleFactor = windowW / oriW;
    //resize
    m_scaleFactor = 1.0;
    scaleImg(scaleFactor);
}

void GraphicsView::fitHeight()
{
    int frameThick = frameWidth() * 2;
    qreal windowH = size().height() - frameThick;
    qreal oriH = imgSize().height();
    qreal scaleFactor = windowH / oriH;
    //resize
    m_scaleFactor = 1.0;
    scaleImg(scaleFactor);
}

void GraphicsView::resetScale()
{
    QTransform matrix;
    m_scaleFactor = 1.0;
    matrix.rotate(m_rotateDegree);
    setTransform(matrix);
}

bool GraphicsView::isAutoResetScale() const
{
    return m_resetScale;
}

void GraphicsView::setAutoResetScale(bool resetScale)
{
    m_resetScale = resetScale;
}

qreal GraphicsView::rotateDegree() const
{
    return m_rotateDegree;
}

void GraphicsView::rotateImg(const qreal &degree)
{
    QTransform matrix;
    m_rotateDegree += degree;
    matrix.rotate(m_rotateDegree);
    matrix.scale(m_scaleFactor, m_scaleFactor);
    setTransform(matrix);
}

void GraphicsView::resetRotate()
{
    QTransform matrix;
    matrix.scale(m_scaleFactor, m_scaleFactor);
    m_rotateDegree = 0.0;
    setTransform(matrix);
}

bool GraphicsView::isAutoResetRotate() const
{
    return m_resetRotate;
}

void GraphicsView::setAutoResetRotate(bool resetRotate)
{
    m_resetRotate = resetRotate;
}

void GraphicsView::resetMatrix()
{
    m_scaleFactor = 1.0;
    m_rotateDegree = 0.0;
    resetTransform();
}

void GraphicsView::autoResetMatrix()
{
    QTransform matrix;
    if(!isAutoResetScale()) {
        matrix.scale(m_scaleFactor, m_scaleFactor);
    }
    else {
        m_scaleFactor = 1.0;
    }
    if(!isAutoResetRotate()) {
        matrix.rotate(m_rotateDegree);
    }
    else {
        m_rotateDegree = 0.0;
    }
    setTransform(matrix);
}

void GraphicsView::resizeEvent(QResizeEvent *e)
{
    QGraphicsView::resizeEvent(e);
    emit resized();
}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
    //prevent scoll move when wheel up/down
    //such function replaced by zoom in/out
    event->ignore();
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    if((event->button() == Qt::LeftButton) &&
            (horizontalScrollBar()->isVisible() || verticalScrollBar()->isVisible())) {
        setCursor(QCursor(Qt::OpenHandCursor));
        event->accept();
    }
    else {
        event->ignore();
        QGraphicsView::mousePressEvent(event);
    }
}

void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        setCursor(QCursor(Qt::ArrowCursor));
        event->accept();
    }
    else {
        event->ignore();
        QGraphicsView::mouseReleaseEvent(event);
    }
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton) {
        if(cursor().shape() == Qt::OpenHandCursor) {
            setCursor(QCursor(Qt::ClosedHandCursor));
        }
        else if(cursor().shape() == Qt::ClosedHandCursor) {
            int x = event->x();
            int y = event->y();
            int dx = x - m_lastX;
            int dy = y - m_lastY;
            if(dx) {
                horizontalScrollBar()->setSliderPosition(horizontalScrollBar()->sliderPosition() - dx);
                m_lastX = x;
            }
            if(dy) {
                verticalScrollBar()->setSliderPosition(verticalScrollBar()->sliderPosition() - dy);
                m_lastY = y;
            }
        }
        event->accept();
    }
    else {
        m_lastX = event->x();
        m_lastY = event->y();
        event->ignore();
        QGraphicsView::mouseMoveEvent(event);
    }
}
