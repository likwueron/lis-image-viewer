/*
 file name: imagelabel.hpp
 author: Li, Kwue-Ron
 e-mail: likwueron@gmail.com
 published under public domain
*/

#ifndef IMAGELABEL_HPP
    #define IMAGELABEL_HPP

#include <QLabel>
class QMovie;

//This comment also give to programmer.
/*
    TRANSLATOR ImageLabel

    ImageLabel is designed for solve the problem that QLabel would not give message after it load an unexist image.
    After loading an unexist image, it's tooltip will be setted as the location of the unexist image.
    And its text will be setted as the tooltip setted before.
    If no tooltip setted, text will be setted as "Image Label".
    Many image in this program use this widget, such as protrait and the images of armors.
*/

class ImageLabel : public QLabel
{
    Q_OBJECT
    Q_PROPERTY(QString imageName READ imgName WRITE setImgFromName)
    Q_PROPERTY(QString toolTip READ toolTip WRITE setToolTip)
public:
    explicit ImageLabel(QWidget *parent = 0);
    explicit ImageLabel(const QString &imgName, const QString &toolTip = "",
               QWidget *parent = 0);
    ~ImageLabel();
    //read and write properties
    QPixmap img() const;
    QString imgName() const;
    void setImgFromName(const QString &imgName);
    void removeImg();
    bool isNull() const;
    bool isLoadErr() const;
    QString errStr() const;

    QString toolTip() const;
    void setToolTip(const QString &toolTip);

    QSize imgSize() const;
signals:
    void resized();
protected:
    void resizeEvent(QResizeEvent *e);
    //functions to do common work. they will called by public fuctions
    void clear();
    void loadImg();
    bool setToolTipIfImgExist();
private:
    QString m_imgName;
    QMovie *mp_movie;
    bool m_loadErr;

    QSize m_imgSize;

    QString m_toolTip;
};

#endif
