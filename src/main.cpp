#ifndef SINGLE_APP
    #include <QApplication>
#else
    #include "core/qtsingleapplication/QtSingleApplication"
#endif
#include <QTranslator>
#include <QLocale>
#ifndef NO_QDEBUG
#include <QDebug>
#endif
#include "mainwindow.hpp"

int main(int argc, char *argv[])
{
#ifndef SINGLE_APP
    QApplication a(argc, argv);
#else
    QtSingleApplication a(argc, argv);
    if(a.isRunning()) {
        QStringList argv = a.arguments();
        if(argv.count() > 1) {
            a.sendMessage(argv.at(1));
        }
        return 0;
    }
#endif
    QApplication::setOrganizationName("Li_Workshop");
    QApplication::setApplicationName("Lis_Image_Viewer");
    QApplication::setApplicationVersion("0.9.0");

    MainWindow w;
    w.show();
#ifdef SINGLE_APP
    w.connect(&a, SIGNAL(messageReceived(QString)), SLOT(openImg(QString)));
    a.setActivationWindow(&w);
#endif
    
    return a.exec();
}
