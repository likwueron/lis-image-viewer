#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "core/langadmin.hpp"
#include "propertydialog.hpp"
#include "imginfomodel.hpp"
#include "core/supext.hpp"
#include "core/filenamegen.hpp"
#include "core/filenamelist.hpp"

#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QPrintDialog>
#include <QPainter>
#include <QGraphicsItem>
#include <QDesktopWidget>
#ifndef NO_QDEBUG
#include <QDebug>
#endif
#include <QWheelEvent>
#include <QDragEnterEvent>
#include <QUrl>

#ifdef Q_OS_WIN32
extern "C" {
    #include <windows.h>
    #include <shellapi.h>
}
#endif

const double MainWindow::sm_maxScale = 8.0;
const double MainWindow::sm_minScale = 0.1;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    mp_ui(new Ui::MainWindow), m_title(tr("Li's Image Viewer")),
    mp_supportFormat(0x0), mp_fileNameGen(0x0), m_state(NoImg),
    mp_model(0x0), mp_propertyDialog(0x0),
    mp_fileList(0x0),
    mp_actLangs(0x0)
{
    mp_ui->setupUi(this);
    mp_ui->graphicsView->setAcceptDrops(false);
    setAcceptDrops(true);

    initLang();

    setupSupExt();

    setupNameList();

    mp_fileNameGen = new FileNameGen(this);
    mp_fileNameGen->setFileNameFormat("\%n - Copy (\%c).\%e");

    readSettings();

    QStringList args = QApplication::arguments();
    if(args.count() > 1) {
        QStringList imgs;
#ifdef Q_OS_WIN32
        int argc = args.count();
        //replace antislash...
        for(int i = 1; i < argc; i++) {
            QString arg = args.at(1);
            arg.replace("\\", "/");
            imgs << arg;
        }
#endif
        openImg(imgs.at(0));
    }
}

MainWindow::~MainWindow()
{
    delete mp_ui;
}

void MainWindow::resetTitle(const QString &fileName)
{
    setWindowTitle(QString("%1 - %2").arg(fileName).arg(m_title));
}

void MainWindow::readSettings()
{
    QSettings settings;
    resize(settings.value("size", QSize(640, 480)).toSize());
    bool isMax = settings.value("maximized", false).toBool();
    if(isMax) {
        showMaximized();
    }
    mp_ui->actionSetting_smartFitToWindow->setChecked(
                settings.value("smartFitToWindow", true).toBool());
    mp_ui->actSet_resetScale->setChecked(
                settings.value("autoResetScale", true).toBool());
    mp_ui->actSet_resetRotate->setChecked(
                settings.value("autoResetRotate", true).toBool());

    initLang();//Prevent error if change statement order in constructor
    QString loc = settings.value("language", QString("actLang_auto")).toString();
    if(LangAdmin::avaliableLangs().contains(loc)) {
        QList<QAction *> actLangList = mp_actLangs->actions();
        foreach(QAction *p_a, actLangList) {
            if(p_a->objectName() == loc) {
                p_a->trigger();
                break;
            }
        }
    }
    else if(loc == "actLang_default") {
        mp_ui->actLang_default->trigger();
    }
    else { //auto
        mp_ui->actLang_auto->trigger();
    }
}

void MainWindow::writeSettings()
{
    QSettings settings;
    bool isMax = isMaximized();
    settings.setValue("maximized", isMax);
    if(isMax) {
        settings.setValue("size", m_normalSize);
    }
    else {
        settings.setValue("size", size());
    }
    settings.setValue("smartFitToWindow",
                      mp_ui->actionSetting_smartFitToWindow->isChecked());
    settings.setValue("autoResetScale",
                      mp_ui->actSet_resetScale->isChecked());
    settings.setValue("autoResetRotate",
                      mp_ui->actSet_resetRotate->isChecked());
    settings.setValue("language",
                      mp_actLangs->checkedAction()->objectName());
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    writeSettings();
    e->accept();
}

void MainWindow::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
    {
        mp_ui->retranslateUi(this);
        m_title = tr("Li's Image Viewer");
        if(mp_model) {
            mp_model->retranslate();
            resetTitle(mp_fileList->fileName());
        }
        e->accept();
        break;
    }
    default:
        e->ignore();
        QMainWindow::changeEvent(e);
        break;
    }
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    if(windowState() & Qt::WindowMaximized) {
        m_normalSize = e->oldSize();
        e->accept();
    }
    QMainWindow::resizeEvent(e);
}

void MainWindow::wheelEvent(QWheelEvent *e)
{
    e->ignore();
    if(!mp_ui->graphicsView->isNull()) {
        QRect region = mp_ui->centralWidget->geometry();
        int x1 = region.left();
        int x2 = region.right();
        int y1 = region.top();
        int y2 = region.bottom();

        int x = e->pos().x();
        int y = e->pos().y();

        if( (x1 < x) && (y1 < y) &&
                (x < x2) && (y < y2) ) { //within graphicsView
            if(e->orientation() == Qt::Vertical) {
                int delta = e->delta();
                if(delta > 0) { //wheel up
                    e->accept();
                    if(mp_ui->actionZoomIn->isEnabled())
                        on_actionZoomIn_triggered();
                }
                else if(delta < 0) { //wheel down
                    e->accept();
                    if(mp_ui->actionZoomOut->isEnabled())
                        on_actionZoomOut_triggered();
                }
            }
        }
    }
    if(!e->isAccepted()) {
        QMainWindow::wheelEvent(e);
    }
}

bool MainWindow::isState(MainWindow::State state) const
{
    return (state == m_state);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    QUrl url = event->mimeData()->urls().first();
    if(url.isLocalFile()) {
        QString fileName = url.toLocalFile();\
        QString ext = fileName.section('.', -1); //string after .; from right to left
        if(!mp_supportFormat->label(ext).isEmpty()) {
            event->acceptProposedAction();
        }
    }
}

void MainWindow::dropEvent(QDropEvent *event)
{
    QUrl url = event->mimeData()->urls().first();
    QString fileName = url.toLocalFile();

    openImg(fileName);
}

void MainWindow::initLang()
{
    if(LangAdmin::inst()) {
        return;
    }
    LangAdmin::creatInst(this);
    LangAdmin::readLangConfig("language.llc");

    mp_actLangs = new QActionGroup(this);
    mp_actLangs->setExclusive(true);
    mp_actLangs->addAction(mp_ui->actLang_auto);
    mp_actLangs->addAction(mp_ui->actLang_default);

    QStringList avaliableLangs = LangAdmin::avaliableLangs();
    foreach(QString loc, avaliableLangs) {
        QAction *p_a= new QAction(this);
        QString lbl = LangAdmin::lblFromLoc(loc);
        p_a->setText(lbl);
        p_a->setObjectName(loc);
        p_a->setCheckable(true);
        mp_ui->menuLang->addAction(p_a);
        mp_actLangs->addAction(p_a);
    }
}

void MainWindow::setupSupExt()
{
    mp_supportFormat = new SupExt(this);
    mp_supportFormat->setAllFileLabel("All Images");
    mp_supportFormat->add("Bitmap", QStringList() << "dib" << "bmp");
    mp_supportFormat->add("GIF", "gif");
    mp_supportFormat->add("JPEG", QStringList() << "jfif" << "jpe" << "jpeg" << "jpg");
    mp_supportFormat->add("JPEG 2000", "jp2");
    mp_supportFormat->add("PNG", "png");
    mp_supportFormat->add("TIFF", QStringList() << "tiff" << "tif");
}

void MainWindow::setupNameList()
{
    if(!mp_supportFormat) {
        setupSupExt();
    }
    mp_fileList = new FileNameList(this);
    mp_fileList->setNameFilter(mp_supportFormat->listFilter());

    connect(mp_fileList, SIGNAL(directoryChanged(QString,int)), SLOT(when_fileNameListUpdated(QString, int)));
}

void MainWindow::openImg(const QString &fileName)
{
    mp_fileList->setDir(fileName);
    //get info
    updateImgInfo(fileName);
    //load img
    mp_ui->graphicsView->setImgFromName(fileName);
    updateActions();
    if(mp_ui->graphicsView->isNull()) {
        return;
    }

    //in state FTW, do FTW
    if(mp_ui->actionFitToWindow->isChecked()) {
        fitToWindow();
    }
}

void MainWindow::updateActions()
{
    //NoImg <-> HasImg
    int imgNum = mp_fileList->count();
    bool hasImg = !mp_ui->graphicsView->isNull();
    mp_ui->actionConvert->setEnabled(hasImg);
    mp_ui->actionDel->setEnabled(hasImg);
    mp_ui->actionDuplicate->setEnabled(hasImg);

    mp_ui->actionPrint->setEnabled(hasImg);

    mp_ui->actionProperties->setEnabled(hasImg);

    mp_ui->actRotateLeft->setEnabled(hasImg);
    mp_ui->actRotateRight->setEnabled(hasImg);
    //everytime open img, check if there multi-img in same dir
    bool multiImg = (imgNum > 1) ? true : false;
    mp_ui->actionNextImg->setEnabled(multiImg);
    mp_ui->actionPreImg->setEnabled(multiImg);

    bool isFtw = mp_ui->actionFitToWindow->isChecked();
    qreal scaleFactor = mp_ui->graphicsView->scaleFactor();
    bool zoomInEnable = (scaleFactor < sm_maxScale);
    bool zoomOutEnable = (scaleFactor > sm_minScale);
    mp_ui->actionFitToWindow->setEnabled(hasImg);
    mp_ui->actionNormalSize->setEnabled(hasImg && !isFtw);
    mp_ui->actionZoomIn->setEnabled(hasImg && zoomInEnable);
    mp_ui->actionZoomOut->setEnabled(hasImg && zoomOutEnable);
}

bool MainWindow::updateImgInfo(const QString &fileName)
{
    if(fileName.isEmpty()) {
        return false;
    }

    if(!mp_model) {
        mp_model = new ImgInfoModel(this);
        connect(mp_model, SIGNAL(fileChanged(QString)), SLOT(resetTitle(QString)));
    }
    mp_model->setFile(fileName);
    return true;
}

void MainWindow::fitToWindow()
{
    if(mp_ui->graphicsView->isNull()) {
        return;
    }

    QSizeF imgSize =  mp_ui->graphicsView->imgSize();
    QSize wSize = mp_ui->centralWidget->size();
    qreal imgW = imgSize.width();
    qreal imgH = imgSize.height();
    int wW = wSize.width();
    int wH = wSize.height();
    bool smartFTW = mp_ui->actionSetting_smartFitToWindow->isChecked();
    bool wIsLarger = (imgW <= wW && imgH <= wH);
    //resize
    if( smartFTW && wIsLarger ) {
        mp_ui->graphicsView->resetScale();
    }
    else {
        mp_ui->graphicsView->fitIn();
    }
}

void MainWindow::scaleImage(const qreal &factor)
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
    mp_ui->graphicsView->scaleImg(factor);

    mp_ui->actionFitToWindow->setChecked(false);
    updateActions();
}

void MainWindow::rotateImage(const qreal &degree)
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
    mp_ui->graphicsView->rotateImg(degree);
    if(mp_ui->actionFitToWindow->isChecked()) {
        fitToWindow();
    }
}

void MainWindow::on_actionFitToWindow_triggered(bool checked)
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
    if (checked) {
        fitToWindow();
        updateActions();
    }
    else {
        on_actionNormalSize_triggered();
    }
}

void MainWindow::on_actionNormalSize_triggered()
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
    mp_ui->graphicsView->resetScale();
    updateActions();
}

void MainWindow::on_actionZoomIn_triggered()
{
    scaleImage(1.25);
}

void MainWindow::on_actionZoomOut_triggered()
{
    scaleImage(0.8);
}

void MainWindow::on_graphicsView_resized()
{
    if(mp_ui->actionFitToWindow->isChecked()) {
        fitToWindow();
        updateActions();
    }
}

void MainWindow::on_actionNextImg_triggered()
{
    QString targetName = mp_fileList->next();
    openImg(QString("%1/%2").arg(mp_model->path(), targetName));
}

void MainWindow::on_actionPreImg_triggered()
{
    QString targetName = mp_fileList->previous();
    openImg(QString("%1/%2").arg(mp_model->path(), targetName));
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), (mp_model ? mp_model->path() : QString()),
                                                    mp_supportFormat->openFileFilter());
    if(fileName.isEmpty()) {
        return;
    }
    openImg(fileName);
}

void MainWindow::on_actionConvert_triggered()
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());

    QString fileName = QFileDialog::getSaveFileName(this, tr("Convert Image"),
                                                    mp_fileList->fileName(),
                                                    mp_supportFormat->openFileFilter());
    if(fileName.isEmpty()) {
        return;
    }

    QPixmap pix = mp_ui->graphicsView->pixmap();
    QString dir = mp_model->path();
    if(!pix.save(fileName)) {
        QString ext = fileName.section('.', -1);
        QString lbl = mp_supportFormat->label(ext);
        QMessageBox::warning(this, tr("Convert Image failure!"),
                             tr("Cannot convert image in \"%1\" as \"%2\" file.")
                             .arg(dir).arg(lbl));
        return;
    }
    else {
        QFileInfo newInfo(fileName);
        QString newDir = newInfo.absolutePath();
        if(dir == newDir) {
            updateActions();
        }
    }
}

void MainWindow::on_actionDel_triggered()
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
#ifdef Q_OS_WIN32
    //use windows insystem function
    QString filepath = mp_ui->graphicsView->imgName();
    filepath += '\0';

    SHFILEOPSTRUCT FileOPStruct;
    FileOPStruct.hwnd=NULL;
    FileOPStruct.wFunc=FO_DELETE;
    FileOPStruct.pFrom=LPCWSTR(filepath.toStdWString().c_str());
    FileOPStruct.pTo=NULL;
    FileOPStruct.fFlags= FOF_ALLOWUNDO | FOF_SILENT;
    FileOPStruct.hNameMappings=NULL;
    FileOPStruct.lpszProgressTitle=NULL;

    if(!SHFileOperation(&FileOPStruct)) {
        //delete success
        //delete name from list
        //open next
        if(mp_fileList->count()) {
            openImg(mp_fileList->fullFilePath());
            //on_actionNextImg_triggered();
        }
        else {
            mp_ui->graphicsView->removeImg();
            setWindowTitle(m_title);

            updateActions();
        }
    }
#else
    QMessageBox::information(this, tr("Cannot Delete"),
                             tr("Delete function is not supported on this OS.\nSorry for the inconvenience."));
    qDebug() << "Not Support Delete Function in this OS!";
#endif
}

void MainWindow::on_actionDuplicate_triggered()
{
    Q_ASSERT(!mp_ui->graphicsView->isNull());
    QString srcName = mp_ui->graphicsView->imgName();

    QString defaultName = mp_fileNameGen->getFileName(mp_fileList->fileName(),
                                                      mp_fileList->list(),
                                                      mp_model->path());
    QString fileName = QFileDialog::getSaveFileName(this, tr("Duplicate"),
                                                    defaultName,
                                                    //imgInfo provide full description like "PNG FILE" which cannot be used in another function
                                                    mp_supportFormat->openFileFilterByExt(mp_model->type()));

    if(fileName.isEmpty()) {
        return;
    }
    //read file
    QFile srcFile(srcName);
    if(!srcFile.open(QIODevice::ReadOnly)) {
        return;
    }
    //write file
    QFile targetFile(fileName);
    int slashPos = fileName.lastIndexOf("/");
    QString dir = fileName.left(slashPos);
    if(!targetFile.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("Duplicate failure!"),
                             tr("Cannot Duplicate image in \"%1\"").arg(dir));
        return;
    }
    targetFile.write(srcFile.readAll());
    //update if need!
    if(mp_model->path() == dir) {
        updateActions();
    }
}

void MainWindow::on_actionPrint_triggered()
{//see in future version
    QMessageBox::information(this, tr("Cannot Print"),
                             tr("This function hasn't implanted yet.\nSorry for the inconvenience."));
    return;

    Q_ASSERT(!mp_ui->graphicsView->isNull());
    QPrintDialog dialog(&m_printer, this);
    dialog.setOptions(QAbstractPrintDialog::PrintSelection | QAbstractPrintDialog::PrintPageRange |
                      QAbstractPrintDialog::PrintShowPageSize);
    if (dialog.exec()) {
        QPainter painter(&m_printer);
        QRect rect = painter.viewport();
        QSizeF size = mp_ui->graphicsView->imgSize();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(mp_ui->graphicsView->sceneRect().toRect());

        mp_ui->graphicsView->itemAt(0,0)->paint(&painter, 0x0);
        //painter.drawPixmap(0, 0, *mp_ui->graphicsView->itemAt());
    }
}

void MainWindow::on_actionProperties_triggered()
{
    if(!mp_propertyDialog) {
        mp_propertyDialog = new PropertyDialog(this);
        mp_propertyDialog->setModel(mp_model);
    }
    mp_propertyDialog->show();

    QDesktopWidget *p_desktop = QApplication::desktop();
    //int resolutionW = p_desktop->screenGeometry(this).width();
    //int screenX = p_desktop->availableGeometry(this).x();
    QRect thisRect = frameGeometry();
    int x = thisRect.x();
    int y = thisRect.y();
    int w = thisRect.width();
    int dialogW = mp_propertyDialog->frameGeometry().width();

    int leftSpace = x - //screenX;
            p_desktop->availableGeometry(this).x();
    int rightSpace = //resolutionW
            p_desktop->screenGeometry(this).width() -
            (x + w);

    QPoint targetPoint = mp_propertyDialog->pos();
    if(dialogW < leftSpace) {
        targetPoint.ry() = y;
        targetPoint.rx() = x - dialogW;
    }
    else if(dialogW < rightSpace) {
        targetPoint.ry() = y;
        targetPoint.rx() = x + w;
    }
    mp_propertyDialog->move(targetPoint);


}

void MainWindow::on_menuLang_triggered(QAction *p_act)
{
    if(p_act == mp_ui->actLang_default) {
        LangAdmin::rmLang();
        return;
    }

    QString loc;
    if(p_act != mp_ui->actLang_auto) {
        QString lbl = p_act->text();
        loc = LangAdmin::locFromLbl(lbl);
    }
    else {//auto detect
        loc = LangAdmin::fuzzyLoc(QLocale::system().name());
    }

    LangAdmin::setLang(loc);
}

void MainWindow::on_actionThis_triggered()
{

#ifdef SINGLE_APP
    QString buildType = "SingleApp";
#else
    QString buildType = "Normal";
#endif
    QMessageBox::about(this, tr("About Li's Image Viewer"),
                       tr("<p>The <b>Li's Image Viewer</b> is designed for better image viewing experience.<br>"
                          "Use it freely if you like it!</p>"
                          "<p align=\"right\">Author: Li, Kwue-Ron<br>"
                          "Email: likwueron@gmail.com<br>"
                          "Version: %1_%2</p>")
                       .arg(QApplication::applicationVersion())
                       .arg(buildType));
}

void MainWindow::on_actionQt_triggered()
{
    QApplication::aboutQt();
}

void MainWindow::on_actionSetting_smartFitToWindow_triggered()
{
    if(mp_ui->actionFitToWindow->isChecked()) {
        fitToWindow();
    }
}

void MainWindow::on_actRotateLeft_triggered()
{
    rotateImage(-90.0);
}

void MainWindow::on_actRotateRight_triggered()
{
    rotateImage(90.0);
}

void MainWindow::on_actSet_resetScale_toggled(bool checked)
{
    mp_ui->graphicsView->setAutoResetScale(checked);
}

void MainWindow::on_actSet_resetRotate_toggled(bool checked)
{
    mp_ui->graphicsView->setAutoResetRotate(checked);
}

void MainWindow::when_fileNameListUpdated(QString, int type)
{
    switch (type) {
    case FileNameList::Remove:
        openImg(mp_fileList->fullFilePath());
        break;
    case FileNameList::Add:
    case FileNameList::NewDirectory:
    case FileNameList::NewPosition:
    default:
        break;
    }
}
