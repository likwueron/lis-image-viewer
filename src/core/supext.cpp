#include "supext.hpp"
#ifndef NO_QDEBUG
#include <QDebug>
#endif

SupExt::SupExt(QObject *parent) :
    QObject(parent)
{
}

SupExt::~SupExt()
{
}

QString SupExt::allFileLabel() const
{
    return m_allFileLabel;
}

void SupExt::setAllFileLabel(const QString &lbl)
{
    m_allFileLabel = lbl;
}

void SupExt::add(const QString &lbl, const QString &ext)
{
    if(lbl.isEmpty() || ext.isEmpty()) {
        return;
    }
    m_extMap.insert(lbl, ext);
}

void SupExt::add(const QString &lbl, const QStringList &exts)
{
    if(lbl.isEmpty() || exts.isEmpty()) {
        return;
    }
    int num = exts.size();
    for(int i = 0; i < num; i++) {
        //inserMulti() reverse input order
        m_extMap.insert(lbl, exts.at(i));
    }
}

QStringList SupExt::listFilter() const
{
    QMapIterator<QString, QString> i(m_extMap);
    QStringList extList;
    while(i.hasNext()) {
        i.next();
        extList << "*." + i.value();
    }

    return extList;
}

QStringList SupExt::listFilterByLbl(const QString &lbl) const
{
    QStringList exts = m_extMap.values(lbl);
    if(exts.empty()) {
        return QStringList();
    }

    int num = exts.size();
    QStringList filter;
    for(int i = 0; i < num; i++) {
        filter << QString("*.%1").arg(exts.at(i));
    }
    return filter;
}

QString SupExt::openFileFilter() const
{
    if(m_extMap.isEmpty()) {
        return QString();
    }

    QString filter;
    QString allFileFilter = "";
    bool hasAllFileLbl = !m_allFileLabel.isEmpty();
    if(hasAllFileLbl) {
        allFileFilter += m_allFileLabel + " (";
    }

    QString curLbl;
    QMapIterator<QString, QString> i(m_extMap);
    while(i.hasNext()) {
        i.next();
        if(curLbl.isEmpty()) {
            curLbl = i.key();
            filter += QString("%1 (*.%2").arg(curLbl).arg(i.value());
        }
        else {
            if(curLbl != i.key()) {
                //new lbl
                curLbl = i.key();
                filter += QString(");;%1 (*.%2").arg(curLbl).arg(i.value());
            }
            else {
                //new ext
                filter += QString(" *.%1").arg(i.value());
            }
        }
        if(hasAllFileLbl) {
            allFileFilter += QString("*.%1 ").arg(i.value());
        }
    }
    //add end
    filter += ")";
    if(hasAllFileLbl) {
        allFileFilter += ");;";
        filter = allFileFilter + filter;
    }

    return filter;
    //All Images (*.bmp *.gif *.jpg *.png *.tif)
}

QString SupExt::openFileFilterByLbl(const QString &lbl) const
{
    QStringList exts = m_extMap.values(lbl);
    if(exts.empty()) {
        return QString();
    }

    int num = exts.size();
    QString filter = lbl + " (";
    for(int i = 0; i < num; i++) {
        filter += QString("*.%1 ").arg(exts.at(i));
    }
    filter += ")";
    return filter;
}

QString SupExt::openFileFilterByExt(const QString &ext) const
{
    QString lbl = m_extMap.key(ext.toLower());
    if(lbl.isEmpty()) {
        return QString();
    }
    return QString("%1 (*.%2)").arg(lbl, ext);
}

QString SupExt::label(const QString &ext)
{
    return m_extMap.key(ext.toLower());
}

QStringList SupExt::extensions(const QString &lbl)
{
    return m_extMap.values(lbl.toLower());
}
