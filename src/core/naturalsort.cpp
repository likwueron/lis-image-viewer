#include "naturalsort.hpp"
#include <QtAlgorithms>
#ifndef NO_QDEBUG
#include <QDebug>
#endif
extern "C" {
#include "strnatcmp.h"
}

bool isLesserThan_natural(const QString &s1, const QString &s2)
{
    int result = strnatcmp(s1.toLocal8Bit(), s2.toLocal8Bit());
    if(result == -1) {
        return true;
    }
    else {
        return false;
    }
}

void naturalStrSort(QStringList &list, NaturalSortFlags flags)
{
    Q_UNUSED(flags)
    /* ignored flags first
    if(flags | Reversed) {

    }
    */
    qSort(list.begin(), list.end(), isLesserThan_natural);
}
