#ifndef FILENAMEGEN_HPP
#define FILENAMEGEN_HPP

#include <QObject>
#include <QStringList>

class FileNameGen : public QObject
{
public:
    explicit FileNameGen(QObject *parent = 0);
    virtual ~FileNameGen();

    QString fileNameFormat() const;
    void setFileNameFormat(const QString &format);

    QString getFileName(const QString &wantedName, const QStringList &existedNames,
                        const QString &path = QString());
private:
    QString m_fileNameFormat;
};

#endif // FILENAMEGEN_HPP
