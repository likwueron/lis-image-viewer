/*
 *File name: filenamelist.cpp
 *Author: Li, Kwue-Ron
 *E-mail: likwueron@gmail.com
 *Last revised: 2013/7/24
 *Published under Public Domain
 */

#include "filenamelist.hpp"
#include <QFileSystemWatcher>
#include <QDir>
#include "naturalsort.hpp"

FileNameList::FileNameList(QObject *parent) :
    QObject(parent), mp_watcher(0x0), m_curPos(0)
{
    mp_watcher = new QFileSystemWatcher(this);
    connect(mp_watcher, SIGNAL(directoryChanged(QString)), this, SLOT(resetNameList()));
}

bool FileNameList::setDir(const QString &path)
{
    //check if it is a directory
    QFileInfo file(path);
    bool isDir = file.isDir();
    QString _path = isDir ? path : file.dir().absolutePath();

    QStringList watchingList = mp_watcher->directories();
    //already in watching list
    QString curName = isDir ? QString() : file.fileName();
    if(watchingList.contains(_path)) {
        //get new pos if dir no change
        m_curPos = m_nameList.indexOf(curName);
        emit directoryChanged(curName, FileNameList::NewPosition);
        return false;
    }

    //remove previous watching directory
    if(!watchingList.isEmpty()) {
        mp_watcher->removePaths(watchingList);
    }
    mp_watcher->addPath(_path);

    //get new dir data
    QDir dir(_path);
    m_nameList = dir.entryList(m_nameFilter, QDir::NoDotAndDotDot | QDir::Files);
    naturalStrSort(m_nameList);

    m_curPos = m_nameList.indexOf(curName);
    if(m_curPos == -1) { //if no contain, then set 0
        m_curPos = 0;
    }
    emit directoryChanged(curName, FileNameList::NewDirectory);
    return true;
}

QStringList FileNameList::nameFilter() const
{
    return m_nameFilter;
}

void FileNameList::setNameFilter(const QStringList &filter)
{
    m_nameFilter = filter;
}

QString FileNameList::fileName() const
{
    return m_nameList.at(m_curPos);
}

QString FileNameList::watchingDir() const
{
    QStringList list = mp_watcher->directories();
    if(list.isEmpty()) {
        return QString();
    }
    else {
        return list.at(0);
    }
}

QString FileNameList::fullFilePath() const
{
    QStringList list = mp_watcher->directories();
    if(list.isEmpty()) {
        return QString();
    }
    else {
        QString dir = list.at(0);
        QString file = fileName();
        return QString("%1/%2").arg(dir, file);
    }
}

QString FileNameList::next()
{
    int count = m_nameList.count();
    if(!count) {
        return QString();
    }

    if(count <= ++m_curPos ) {
        m_curPos = 0;
    }
    return m_nameList.at(m_curPos);
}

QString FileNameList::previous()
{
    int count = m_nameList.count();
    if(!count) {
        return QString();
    }

    if(--m_curPos < 0) {
        m_curPos = count - 1;
    }
    return m_nameList.at(m_curPos);
}

int FileNameList::count() const
{
    return m_nameList.count();
}

QStringList FileNameList::list() const
{
    return m_nameList;
}

void FileNameList::resetNameList()
{
    QStringList watchingList = mp_watcher->directories();
    if(watchingList.isEmpty()) {
        return;
    }

    //get old data
    QString oldName = m_nameList.isEmpty() ? QString() : m_nameList.at(m_curPos);
    QStringList oldList = m_nameList;
    //update
    QDir dir(watchingList.at(0));
    m_nameList = dir.entryList(m_nameFilter, QDir::NoDotAndDotDot | QDir::Files);
    naturalStrSort(m_nameList);

    //check if added
    int num = qMin(oldList.count(), m_nameList.count());
    QString oldStr, newStr;
    for(int i = 0; i < num; i++) {
        oldStr = oldList.at(i);
        newStr = m_nameList.at(i);
        if(oldStr != newStr) {
            if(isLesserThan_natural(newStr, oldStr)) {
                m_curPos += 1;
            }
            emit directoryChanged(newStr, FileNameList::Add);
            break;
        }
    }
    //check if removed
    if(!m_nameList.contains(oldName)) {
        //removed
        if(m_nameList.count() <= m_curPos) {
            m_curPos = 0;
        }
        emit directoryChanged(oldName, FileNameList::Remove);
    }
}
