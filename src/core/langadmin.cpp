/*
 *File name: langadmin.cpp
 *Author: Li, Kwue-Ron
 *E-mail: likwueron@gmail.com
 *Last revised: 03/18/2013
 *Published under Public Domain
 */

#include "langadmin.hpp"
#include <QCoreApplication>
#include <QTranslator>
#include <QFile>

LangAdmin *LangAdmin::smp_inst = 0x0;
QMutex LangAdmin::sm_mutex;

QString LangAdmin::sm_langPath;
QMap<QString, QString> LangAdmin::sm_langMap;
QString LangAdmin::sm_curLoc;
QList<QTranslator*> LangAdmin::sm_curTrFiles;

bool LangAdmin::creatInst(QObject *parent)
{
    if(smp_inst) {
        return true;
    }
    if(sm_mutex.tryLock()) {
        smp_inst = new LangAdmin(parent);
        sm_mutex.unlock();
        return true;
    }
    else {
        return false;
    }
}

LangAdmin *LangAdmin::inst()
{
    return smp_inst;
}

LangAdmin::~LangAdmin()
{
    smp_inst = 0x0;
    foreach(QTranslator *p_tr, sm_curTrFiles) {
        delete p_tr;
    }
}

QString LangAdmin::langPath()
{
    return sm_langPath;
}

void LangAdmin::setLangPath(const QDir &dir)
{
    setLangPath(dir.path());
}

void LangAdmin::setLangPath(const QString &path)
{
    sm_langPath = absPathFromAppPath(path);
}

QString LangAdmin::absPathFromAppPath(const QString &path)
{
    QDir dir(path);
    if(dir.isAbsolute()) {
        return path;
    }
    else {
        QString pathFromDir = dir.path();
        if(pathFromDir.startsWith("./")) {
            pathFromDir = pathFromDir.right(pathFromDir.count() - 1);
        }
        else {
            pathFromDir.prepend('/');
        }
        return QString(QCoreApplication::applicationDirPath() + pathFromDir);
    }
}

void LangAdmin::readLangConfig(const QString &fileName)
{
    if(sm_langMap.count()) {
        sm_langMap.clear();
        sm_langPath.clear();
    }

    QString srcFileName = absPathFromAppPath(fileName);

    QFile srcFile(srcFileName);
    if(!srcFile.open(QIODevice::ReadOnly)) {
        return;
    }

    while(!srcFile.atEnd()) {
        QString line = QString::fromUtf8(srcFile.readLine());
        //ignore comments
        if(line.startsWith('#')) {
            continue;
        }
        //get values
        QStringList part = line.split('\t');
        QString name = part.at(0).simplified();
        QString value = part.at(1).simplified();
        //sm_langPath = LANGDIR
        if(name == "LANGDIR") {
            setLangPath(value);
            continue;
        }
        //set locale-language label map
        sm_langMap.insert(name, value);
    }
    //auto set sm_langPath
    if(sm_langPath.isEmpty()) {
        setLangPath("./language");
    }
}

QStringList LangAdmin::avaliableLangs()
{
    QDir langDir(sm_langPath);
    QStringList list = langDir.entryList(QDir::Dirs | QDir::Readable | QDir::NoDotAndDotDot);
    QStringList result;
    foreach(QString loc, list) {
        QDir dir = langDir;
        dir.cd(loc);
        //whether this dir contains *.qm files
        if(dir.entryList(QStringList() << "*.qm").count()) {
            result << loc;
        }
    }

    return result;
}

QString LangAdmin::lang()
{
    return sm_curLoc;
}

void LangAdmin::setLang(const QString &loc)
{
    if(loc.isEmpty()) {
        return;
    }
    if(sm_curLoc == loc) {
        return;
    }
    //remove old translators
    if(sm_curTrFiles.count()) {
        rmLang();
    }

    QDir dir = sm_langPath;
    if(dir.cd(loc)) {
        QFileInfoList entries = dir.entryInfoList(QStringList() << "*.qm",
                                                  QDir::Files | QDir::Readable);
        //load new translators
        foreach(QFileInfo file, entries) {
            QTranslator *p_tr = new QTranslator;
            if(p_tr->load(file.absoluteFilePath())) {
                QCoreApplication::installTranslator(p_tr);
                sm_curTrFiles << p_tr;
            }
        }

        sm_curLoc = loc;
    }
}

void LangAdmin::rmLang()
{
    foreach(QTranslator *p_tr, sm_curTrFiles) {
        QCoreApplication::removeTranslator(p_tr);
        delete p_tr;
    }
    sm_curTrFiles.clear();
    sm_curLoc.clear();
}

QString LangAdmin::lblFromLoc(const QString &loc)
{
    return sm_langMap.value(loc, loc);
}

QString LangAdmin::locFromLbl(const QString &lbl)
{
    return sm_langMap.key(lbl, lbl);
}

QString LangAdmin::fuzzyLoc(const QString &loc)
{
    QDir dir(sm_langPath);
    if(dir.cd(loc)) {
        if(dir.entryList(QStringList() << "*.qm").count()) {
            return loc;
        }
    }
    int dashPos = loc.indexOf('_');
    return loc.left(dashPos);
}

LangAdmin::LangAdmin(QObject *parent) :
    QObject(parent)
{

}
