#ifndef SUPEXT_HPP
#define SUPEXT_HPP

#include <QObject>
#include <QStringList>
#include <QMultiMap>

class SupExt : public QObject
{
    Q_OBJECT
public:
    explicit SupExt(QObject *parent = 0);
    ~SupExt();

    QString allFileLabel() const;
    void setAllFileLabel(const QString &lbl);

    void add(const QString &lbl, const QString &ext);
    void add(const QString &lbl, const QStringList &exts);

    QStringList listFilter() const;
    QStringList listFilterByLbl(const QString &lbl) const;

    QString openFileFilter() const;
    QString openFileFilterByLbl(const QString &lbl) const;
    QString openFileFilterByExt(const QString &ext) const;
    
    QString label(const QString &ext);
    QStringList extensions(const QString &lbl);
signals:
    
public slots:
protected:

private:
    QString m_allFileLabel;
    QMultiMap<QString, QString> m_extMap;
};

#endif // SUPEXT_HPP
