/*
 *File name: langadmin.cpp
 *Author: Li, Kwue-Ron
 *E-mail: likwueron@gmail.com
 *Last revised: 03/18/2013
 *Published under Public Domain
 */

#ifndef LANGADMIN_HPP
#define LANGADMIN_HPP

#include <QObject>
#include <QMutex>
#include <QMap>
#include <QDir>
#include <QStringList>

class QTranslator;

class LangAdmin : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString langPath READ langPath WRITE setLangPath)
    Q_PROPERTY(QString lang READ lang WRITE setLang)
public:
    static bool creatInst(QObject *parent = 0);
    static LangAdmin* inst();
    virtual ~LangAdmin();
    //Path that contains translate files(*.qm files)
    static QString langPath();
    static void setLangPath(const QDir &dir);
    static void setLangPath(const QString &path);
    //Make all relative path be absolute path accroding to path of application
    static QString absPathFromAppPath(const QString &path);
    //read the configuration file
    static void readLangConfig(const QString &fileName = "./language.llc");
    static QStringList avaliableLangs();
    //Set language the application used by locale
    static QString lang();
    static void setLang(const QString &loc);
    static void rmLang();
    //Converse between label (ex: Ameriacan English) and locale (ex: en_US)
    static QString lblFromLoc(const QString &loc);
    static QString locFromLbl(const QString &lbl);
    //Return language without country (ex: find no "en_US", return "en")
    //Use config file to determine the return value
    static QString fuzzyLoc(const QString &loc);

private:
    explicit LangAdmin(QObject *parent = 0);

    static LangAdmin *smp_inst;
    static QMutex sm_mutex;

    static QString sm_langPath;
    static QMap<QString, QString> sm_langMap;
    static QString sm_curLoc;
    static QList<QTranslator*> sm_curTrFiles;
};

#endif // LANGADMIN_HPP
