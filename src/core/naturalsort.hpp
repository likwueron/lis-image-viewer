#ifndef NATURALSORT_HPP
#define NATURALSORT_HPP

#include <QStringList>

/*
 *lesser than: true
 *greater or equal: false
 */

enum NaturalSortFlags {
    Name = 0x00,
    Time = 0x01,
    Size = 0x02,
    Type = 0x80,
    //DirsFirst = 0x04,
    //DirsLast = 0x20,
    Reversed = 0x08
};

bool isLesserThan_natural(const QString &s1, const QString &s2);
void naturalStrSort(QStringList &list, NaturalSortFlags flags = Name);

#endif // NATURALSORT_HPP
