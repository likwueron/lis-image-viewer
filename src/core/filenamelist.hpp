/*
 *File name: filenamelist.hpp
 *Author: Li, Kwue-Ron
 *E-mail: likwueron@gmail.com
 *Last revised: 2013/7/24
 *Published under Public Domain
 */

#ifndef FILENAMELIST_HPP
#define FILENAMELIST_HPP

#include <QObject>
#include <QStringList>
class QFileSystemWatcher;

class FileNameList : public QObject
{
    Q_OBJECT
public:
    enum ChangeMode {
        Add,
        Remove,
        NewDirectory,
        NewPosition,
        Others
    };

    explicit FileNameList(QObject *parent = 0);
    
    bool setDir(const QString &path);

    QStringList nameFilter() const;
    void setNameFilter(const QStringList &filter);

    QString fileName() const;
    QString watchingDir() const;
    QString fullFilePath() const;
    QString next();
    QString previous();

    int count() const;
    QStringList list() const;

signals:
    void directoryChanged(QString, int);
public slots:
private:
    QFileSystemWatcher *mp_watcher;
    QStringList m_nameFilter;

    QStringList m_nameList;
    int m_curPos;

private slots:
    void resetNameList();
};

#endif // FILENAMELIST_HPP
