#include "filenamegen.hpp"
#include <QDebug>

FileNameGen::FileNameGen(QObject *parent)
    :QObject(parent)
{
}

FileNameGen::~FileNameGen()
{

}

QString FileNameGen::fileNameFormat() const
{
    return m_fileNameFormat;
}

void FileNameGen::setFileNameFormat(const QString &format)
{
    m_fileNameFormat = format;
}

QString FileNameGen::getFileName(const QString &wantedName, const QStringList &existedNames,
                                 const QString &path)
{
    //in windows -> lower case
    QString partF = wantedName.section('.', 0, 0);
    QString partE = wantedName.section('.', 1);
    QRegExp regex(QString("%1(\\s-\\sCopy\\s\\(\\d+\\))?").arg(partF), Qt::CaseInsensitive);
    //search name match patten
    QStringList existedFs;
    foreach(QString existedName, existedNames) {
        QString existedF = existedName.section('.', 0, 0);
        QString existedE = existedName.section('.', 1);
        if(regex.exactMatch(existedF) &&
                !existedE.compare(partE, Qt::CaseInsensitive)) {
            //existedF match patten; partE == existedE
            existedFs << existedF;
        }
    }
    //rename
    QString tempF = partF;
    int count = 0;
    foreach(QString existedF, existedFs) {
        if(!existedF.compare(tempF, Qt::CaseInsensitive)) {
            //rename
            tempF = QString("%1 - Copy (%2)").arg(partF).arg(++count);
            existedFs.removeOne(existedF);
            break;
        }
    }

    QString result = path;
    if(path.lastIndexOf('/') != path.count()-1) { //if / is not the last char of path
        result += '/';
    }
    result += QString("%1.%2").arg(tempF, partE);

    return result;
}
