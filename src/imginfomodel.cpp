#include "imginfomodel.hpp"
#include <QFileInfo>
#include <QImageReader>
#include <QDateTime>
#ifndef NO_QDEBUG
#include <QDebug>
#endif

ImgInfoModel::ImgInfoModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

ImgInfoModel::ImgInfoModel(const QString &file, QObject *parent) :
    QAbstractTableModel(parent)
{
    setFile(file);
}

ImgInfoModel::~ImgInfoModel()
{
    clear();
}

Qt::ItemFlags ImgInfoModel::flags(const QModelIndex &index) const
{
    if(index.column() != 0) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    }
    return Qt::ItemIsEnabled;
}

/*
QModelIndex ImgInfoModel::index(int row, int column, const QModelIndex &parent) const
{
    return QModelIndex();
}*/
/*
QModelIndex ImgInfoModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}*/

int ImgInfoModel::rowCount(const QModelIndex &) const
{
    return 8;
}

int ImgInfoModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant ImgInfoModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    if(role == Qt::DisplayRole) {
        if(col == 0) {
            switch(row) {
            case 0:
                return tr("File Name:");
            case 1:
                return tr("File Path:");
            case 2:
                return tr("Image Type:");
            case 3:
                return tr("Size:");
            case 4:
                return tr("Date Created:");
            case 5:
                return tr("Date Modified:");
            case 6:
                return tr("Animation:");
            case 7:
                return tr("Comment:");
            }
        }
        else {
            switch(row) {
            case 0:
                return m_name;
            case 1:
                return m_path;
            case 2: {
                if(m_type.isEmpty()) {
                    return tr("Not Supported Format or Broken File");
                }
                else {
                    return tr("%1 FILE").arg(m_type);
                }
            }
            case 3: {
                if(m_size.isValid()) {
                    return QString("%1 X %2 X %3")
                            .arg(m_size.width())
                            .arg(m_size.height())
                            .arg(depthStr());
                }
                else {
                    return tr("Not Supported");
                }
            }
            case 4:
                return m_create;
            case 5:
                return m_modify;
            case 6: {
                if(frameCount()) {
                    return tr("Frame Number: %1, Frame Delay: %2")
                            .arg(m_frameCount)
                            .arg(m_frameDelay);
                }
                else {
                    return tr("Not Supported");
                }
            }
            case 7: {
                if(!m_comment.isEmpty()) {
                    return m_comment;
                }
                else {
                    return tr("Not Supported");
                }
            }
            }

        }
    }
    else if(role == Qt::WhatsThisRole) {
        if(col == 1 && row == 3) {
            return sizeDescript();
        }
    }
    return QVariant();
}

QVariant ImgInfoModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole) {
        if(orientation == Qt::Horizontal) {
            switch(section) {
            case 0:
                return tr("Property");
            case 1:
                return tr("Value");
            }
        }
    }
    return QVariant();
}

void ImgInfoModel::setFile(const QString &file)
{
    mp_fileInfo = new QFileInfo(file);
    mp_imgReader = new QImageReader(file);

    //m_name = mp_fileInfo->completeBaseName();
    m_name = mp_fileInfo->fileName();

    m_path = mp_fileInfo->absolutePath();

    m_type = mp_imgReader->format().toUpper();

    m_size = mp_imgReader->size();

    m_format = mp_imgReader->imageFormat();

    m_create = mp_fileInfo->created().toString(Qt::SystemLocaleDate);

    m_modify = mp_fileInfo->lastModified().toString(Qt::SystemLocaleDate);

    if(mp_imgReader->supportsOption(QImageIOHandler::Description)) {
        m_comment = mp_imgReader->text("Comment");
    }
    else {
        m_comment.clear();
    }

    if(mp_imgReader->supportsAnimation()) {
        m_frameCount = mp_imgReader->imageCount();
        m_frameDelay = mp_imgReader->nextImageDelay();
    }
    else {
        m_frameCount = 0;
        m_frameDelay = 0;
    }

    refreshData();
    emit fileChanged(m_name);
    clear();
}

void ImgInfoModel::retranslate()
{
    refreshData();
    refreshProperty();
    refreshHeaderData();
}

QString ImgInfoModel::name() const
{
    return m_name;
}

QString ImgInfoModel::path() const
{
    return m_path;
}

QString ImgInfoModel::type() const
{
    return m_type;
}

QSize ImgInfoModel::size() const
{
    return m_size;
}

int ImgInfoModel::depth() const
{
    return depthGen(m_format);
}

QString ImgInfoModel::depthStr() const
{
    return depthStrGen(m_format);
}

QString ImgInfoModel::sizeDescript() const
{
    return depthDescriptGen(m_format);
}

QString ImgInfoModel::create() const
{
    return m_create;
}

QString ImgInfoModel::modify() const
{
    return m_modify;
}

QString ImgInfoModel::comment() const
{
    return m_comment;
}

int ImgInfoModel::frameCount() const
{
    return m_frameCount;
}

int ImgInfoModel::frameDelay() const
{
    return m_frameDelay;
}

int ImgInfoModel::depthGen(int format) const
{
    switch(format) {
    case 1:
    case 2:
        return 1;
    case 3:
        return 8;
    case 4:
    case 5:
    case 6:
        return 32;
    case 7:
    case 11:
    case 14:
    case 15:
        return 16;
    case 8:
    case 9:
    case 10:
    case 12:
    case 13:
        return 24;
    default:
        return -1;
    }
}

QString ImgInfoModel::depthStrGen(int format) const
{
    QString depthTemplate = "%1 (%2)";
    QString depthTemplate2 = "%1 (%2 %3)";
    QString preMulti = tr("Premultiplied");

    switch(format) {
    case 1:
        return depthTemplate.arg(1).arg(tr("MSB"));
    case 2:
        return depthTemplate.arg(1).arg(tr("LSB"));
    case 3:
        return depthTemplate.arg(8).arg(tr("Color Map"));
    case 4:
        return depthTemplate.arg(32).arg(tr("nRGB - 8888"));
    case 5:
        return depthTemplate.arg(32).arg(tr("ARGB - 8888"));
    case 6:
        return depthTemplate2.arg(32).arg(preMulti, tr("ARGB - 8888"));
    case 7:
        return depthTemplate.arg(16).arg(tr("RGB - 565"));
    case 8:
        return depthTemplate2.arg(24).arg(preMulti, tr("ARGB - 8565"));
    case 9:
        return depthTemplate.arg(24).arg(tr("nRGB - 6666"));
    case 10:
        return depthTemplate2.arg(24).arg(preMulti, tr("ARGB - 6666"));
    case 11:
        return depthTemplate.arg(16).arg(tr("nRGB - 1555"));
    case 12:
        return depthTemplate2.arg(24).arg(preMulti, tr("AnRGB - 81555"));
    case 13:
        return depthTemplate.arg(24).arg(tr("RGB - 888"));
    case 14:
        return depthTemplate.arg(16).arg(tr("nRGB - 4444"));
    case 15:
        return depthTemplate2.arg(16).arg(preMulti, tr("ARGB - 4444"));
    default:
        return "?";
    }
}

QString ImgInfoModel::depthDescriptGen(int format) const
{
    switch(format) {
    case 1:
        return depthDescriptGen(0, 0, 0, 0, 0, false, 1);
    case 2:
        return depthDescriptGen(0, 0, 0, 0, 0, false, 2);
    case 3:
        return tr("8-bit indexes into a colormap.");
    case 4:
        return depthDescriptGen(8, 0, 8, 8, 8);
    case 5:
        return depthDescriptGen(0, 8, 8, 8, 8);
    case 6:
        return depthDescriptGen(0, 8, 8, 8, 8, true);
    case 7:
        return depthDescriptGen(0, 0, 5, 6, 5);
    case 8:
        return depthDescriptGen(0, 8, 5, 6, 5, true);
    case 9:
        return depthDescriptGen(6, 0, 6, 6, 6);
    case 10:
        return depthDescriptGen(0, 6, 6, 6, 6, true);
    case 11:
        return depthDescriptGen(1, 0, 5, 5, 5);
    case 12:
        return depthDescriptGen(1, 8, 5, 5, 5, true);
    case 13:
        return depthDescriptGen(0, 0, 8, 8, 8);
    case 14:
        return depthDescriptGen(4, 0, 4, 4, 4);
    case 15:
        return depthDescriptGen(0, 4, 4, 4, 4, true);
    default:
        return QString();
    }
}

QString ImgInfoModel::depthDescriptGen(uint nonsense, uint alpha,
                                       uint red, uint green, uint blue,
                                       bool premultiplied, uint noncolorful) const
{
    uint totalBit = nonsense + alpha + red + green + blue;

    QString result = tr("Width X Height X Depth\n%1-bit per pixel.")
            .arg(noncolorful ? 1 : totalBit);

    if(premultiplied && !noncolorful) {
        result += tr("\nUsing special algorithm to accelerate the loading speed.\nMay loss some image quality.");
    }

    if(!noncolorful) {
        result += tr("\nData of each pixel stored as following: ");
        if(alpha) {
            result += tr("\n\tAlpha channel: %1-bit").arg(alpha);
        }
        if(nonsense) {
            result += tr("\n\tnonsense: %1-bit").arg(nonsense);
        }
        if(red) {
            result += tr("\n\tRed: %1-bit").arg(red);
        }
        if(green) {
            result += tr("\n\tGreen: %1-bit").arg(green);
        }
        if(blue) {
            result += tr("\n\tBlue: %1-bit").arg(blue);
        }
    }
    else {
        result += tr("\nBytes are packed with the \"%1\" first.").arg((noncolorful == 1) ?
                                                                        tr("Most Significant Bit") :
                                                                        tr("Less Significant Bit"));
    }

    return result;
}

void ImgInfoModel::refreshData()
{
    QModelIndex topLeft = createIndex(1,0);
    QModelIndex bottomRight = createIndex(1,7);
    emit dataChanged(topLeft, bottomRight);
}

void ImgInfoModel::refreshProperty()
{
    QModelIndex topLeft = createIndex(0,0);
    QModelIndex bottomRight = createIndex(0,7);
    emit dataChanged(topLeft, bottomRight);
}

void ImgInfoModel::refreshHeaderData()
{
    emit headerDataChanged(Qt::Horizontal, 0, 1);
}

void ImgInfoModel::clear()
{
    if(mp_fileInfo) {
        delete mp_fileInfo;
        mp_fileInfo = 0x0;
    }
    if(mp_imgReader) {
        delete mp_imgReader;
        mp_imgReader = 0x0;
    }
}
