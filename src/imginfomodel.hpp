#ifndef IMGINFOMODEL_HPP
#define IMGINFOMODEL_HPP

#include <QAbstractTableModel>
#include <QSize>
#include <QMap>

class QFileInfo;
class QImageReader;

class ImgInfoModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name())
public:
    explicit ImgInfoModel(QObject *parent = 0);
    explicit ImgInfoModel(const QString &file, QObject *parent = 0);
    ~ImgInfoModel();
    
    Qt::ItemFlags flags(const QModelIndex &index) const;
    //QModelIndex index(int row, int column, const QModelIndex &parent) const;
    //QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void setFile(const QString &file);
    void retranslate();
    //I'm lazy
    //void setFile(const QFile &file);
    //void setFile(const QDir &dir, const QString &file);

    QString name() const;
    QString path() const;
    QString type() const;
    QSize size() const;
    int depth() const;
    QString depthStr() const;
    QString sizeDescript() const;
    QString create() const;
    QString modify() const;
    QString comment() const;
    int frameCount() const;
    int frameDelay() const;
signals:
    void fileChanged(QString name);
public slots:
protected:
    int depthGen(int format) const;
    QString depthStrGen(int format) const;
    QString depthDescriptGen(int format) const;
    QString depthDescriptGen(uint, uint, uint, uint, uint, bool premultiplied = false, uint noncolorful= 0) const;

    void refreshData();
    void refreshProperty();
    void refreshHeaderData();
private:
    void clear();

    QFileInfo *mp_fileInfo;
    QImageReader *mp_imgReader;

    QString m_name;
    QString m_path;
    QString m_type;
    QSize m_size;
    int m_format;
    QString m_sizeDescript;
    QString m_create;
    QString m_modify;
    QString m_comment;
    int m_frameCount;
    int m_frameDelay;
};

#endif // IMGINFOMODEL_HPP
