<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>GraphicsView</name>
    <message>
        <location filename="../../src/graphicsview.cpp" line="41"/>
        <source>Can not load &quot;%1&quot; properly.
This file might broken or too large.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageLabel</name>
    <message>
        <location filename="../../src/imagelabel.cpp" line="89"/>
        <source>Cannot load %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.cpp" line="167"/>
        <source>Image Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.cpp" line="179"/>
        <source>Image: &quot;%1&quot; is not exist!</source>
        <extracomment>%1 means the location of the not exist image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.hpp" line="15"/>
        <source></source>
        <comment>ImageLabel is designed for solve the problem that QLabel would not give message after it load an unexist image. After loading an unexist image, it&apos;s tooltip will be setted as the location of the unexist image. And its text will be setted as the tooltip setted before. If no tooltip setted, text will be setted as &quot;Image Label&quot;. Many image in this program use this widget, such as protrait and the images of armors.</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>ImgInfoModel</name>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="43"/>
        <source>File Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="45"/>
        <source>File Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="47"/>
        <source>Image Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="49"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="51"/>
        <source>Date Created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="53"/>
        <source>Date Modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="55"/>
        <source>Animation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="57"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="68"/>
        <source>Not Supported Format or Broken File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="71"/>
        <source>%1 FILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="82"/>
        <location filename="../../src/imginfomodel.cpp" line="96"/>
        <location filename="../../src/imginfomodel.cpp" line="104"/>
        <source>Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="91"/>
        <source>Frame Number: %1, Frame Delay: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="125"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="127"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="273"/>
        <source>Premultiplied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="277"/>
        <source>MSB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="279"/>
        <source>LSB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="281"/>
        <source>Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="283"/>
        <source>nRGB - 8888</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="285"/>
        <location filename="../../src/imginfomodel.cpp" line="287"/>
        <source>ARGB - 8888</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="289"/>
        <source>RGB - 565</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="291"/>
        <source>ARGB - 8565</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="293"/>
        <source>nRGB - 6666</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="295"/>
        <source>ARGB - 6666</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="297"/>
        <source>nRGB - 1555</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="299"/>
        <source>AnRGB - 81555</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="301"/>
        <source>RGB - 888</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="303"/>
        <source>nRGB - 4444</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="305"/>
        <source>ARGB - 4444</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="319"/>
        <source>8-bit indexes into a colormap.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="355"/>
        <source>Width X Height X Depth
%1-bit per pixel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="359"/>
        <source>
Using special algorithm to accelerate the loading speed.
May loss some image quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="363"/>
        <source>
Data of each pixel stored as following: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="365"/>
        <source>
	Alpha channel: %1-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="368"/>
        <source>
	nonsense: %1-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="371"/>
        <source>
	Red: %1-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="374"/>
        <source>
	Green: %1-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="377"/>
        <source>
	Blue: %1-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="381"/>
        <source>
Bytes are packed with the &quot;%1&quot; first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="382"/>
        <source>Most Significant Bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="383"/>
        <source>Less Significant Bit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/mainwindow.ui" line="17"/>
        <location filename="../../src/mainwindow.cpp" line="36"/>
        <location filename="../../src/mainwindow.cpp" line="150"/>
        <source>Li&apos;s Image Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="56"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="70"/>
        <source>&amp;Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="84"/>
        <source>A&amp;bout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="91"/>
        <source>&amp;Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="95"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="130"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="133"/>
        <source>Ctrl+P</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="141"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="144"/>
        <source>Ctrl++</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="152"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="155"/>
        <source>Ctrl+-</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="163"/>
        <source>Normal Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="166"/>
        <source>Ctrl+0</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="180"/>
        <source>Fit to Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="183"/>
        <source>Ctrl+Alt+0</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="191"/>
        <source>Next Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="194"/>
        <source>Right</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="202"/>
        <source>Previous Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="205"/>
        <source>Left</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="210"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="213"/>
        <source>Ctrl+O</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="221"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="224"/>
        <source>Del</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="232"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="235"/>
        <source>Ctrl+I</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="243"/>
        <location filename="../../src/mainwindow.cpp" line="522"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="246"/>
        <source>Ctrl+Alt+C</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="255"/>
        <source>This Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="264"/>
        <source>Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="272"/>
        <source>Convert to...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="275"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="286"/>
        <source>&amp;Smart Fit to Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="294"/>
        <source>Auto Detect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="302"/>
        <source>Program Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="310"/>
        <source>Rotate Counterclockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="313"/>
        <source>Ctrl+Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="321"/>
        <source>Rotate clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="324"/>
        <source>Ctrl+Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="335"/>
        <source>Auto Reset Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="346"/>
        <source>Auto Reset Rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="437"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="449"/>
        <source>Convert Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="461"/>
        <source>Convert Image failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="462"/>
        <source>Cannot convert image in &quot;%1&quot; as &quot;%2&quot; file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="508"/>
        <source>Cannot Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="509"/>
        <source>Delete function is not supported on this OS.
Sorry for the inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="540"/>
        <source>Duplicate failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="541"/>
        <source>Cannot Duplicate image in &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="553"/>
        <source>Cannot Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="554"/>
        <source>This function hasn&apos;t implanted yet.
Sorry for the inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="632"/>
        <source>About Li&apos;s Image Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="633"/>
        <source>&lt;p&gt;The &lt;b&gt;Li&apos;s Image Viewer&lt;/b&gt; is designed for better image viewing experience.&lt;br&gt;Use it freely if you like it!&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Author: Li, Kwue-Ron&lt;br&gt;Email: likwueron@gmail.com&lt;br&gt;Version: %1&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyDialog</name>
    <message>
        <location filename="../../src/propertydialog.ui" line="30"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/propertydialog.cpp" line="44"/>
        <source>Properties of %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
