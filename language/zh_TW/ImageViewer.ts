<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_TW">
<context>
    <name>GraphicsView</name>
    <message>
        <location filename="../../src/graphicsview.cpp" line="41"/>
        <source>Can not load &quot;%1&quot; properly.
This file might broken or too large.</source>
        <translation>無法正確的讀取&quot;%1&quot;。
該檔案可能損毀或太大。</translation>
    </message>
</context>
<context>
    <name>ImageLabel</name>
    <message>
        <location filename="../../src/imagelabel.cpp" line="89"/>
        <source>Cannot load %1.</source>
        <translation>無法讀取%1。</translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.cpp" line="167"/>
        <source>Image Label</source>
        <translation>圖片標籤</translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.cpp" line="179"/>
        <source>Image: &quot;%1&quot; is not exist!</source>
        <extracomment>%1 means the location of the not exist image</extracomment>
        <translation>圖片：&quot;%1&quot;不存在！</translation>
    </message>
    <message>
        <location filename="../../src/imagelabel.hpp" line="15"/>
        <source></source>
        <comment>ImageLabel is designed for solve the problem that QLabel would not give message after it load an unexist image. After loading an unexist image, it&apos;s tooltip will be setted as the location of the unexist image. And its text will be setted as the tooltip setted before. If no tooltip setted, text will be setted as &quot;Image Label&quot;. Many image in this program use this widget, such as protrait and the images of armors.</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>ImgInfo</name>
    <message>
        <source>Not Supported Format or Broken File</source>
        <translation type="obsolete">不支援的格式或損毀的檔案</translation>
    </message>
    <message>
        <source>%1 FILE</source>
        <translation type="obsolete">%1檔</translation>
    </message>
    <message>
        <source>Not Supported</source>
        <translation type="obsolete">不支援</translation>
    </message>
    <message>
        <source>Frame Number: %1, Frame Delay: %2</source>
        <translation type="obsolete">影格數：%1，影格延遲：%2</translation>
    </message>
    <message>
        <source>Width X Height X Depth
%1-bit per pixel.</source>
        <translation type="obsolete">寬度X高度X深度
每像素%1位元。</translation>
    </message>
    <message>
        <source>
Using special algorithm to accelerate the loading speed.
May loss some image quality.</source>
        <translation type="obsolete">
使用特殊演算法加速讀取速度。
可能會損傷圖型品質。</translation>
    </message>
    <message>
        <source>
Data of each pixel stored as following: </source>
        <translation type="obsolete">
每像素的資料儲存如下：</translation>
    </message>
    <message>
        <source>
	Alpha channel: %1-bit</source>
        <translation type="obsolete">
	透明通道：%1位元</translation>
    </message>
    <message>
        <source>
	nonsense: %1-bit</source>
        <translation type="obsolete">
	無意義：%1位元</translation>
    </message>
    <message>
        <source>
	Red: %1-bit</source>
        <translation type="obsolete">
	紅色：%1位元</translation>
    </message>
    <message>
        <source>
	Green: %1-bit</source>
        <translation type="obsolete">
	綠色：%1位元</translation>
    </message>
    <message>
        <source>
	Blue: %1-bit</source>
        <translation type="obsolete">
	藍色: %1位元</translation>
    </message>
    <message>
        <source>
Bytes are packed with the &quot;%1&quot; first.</source>
        <translation type="obsolete">
位元以&quot;%1&quot;優先的方式存儲。</translation>
    </message>
    <message>
        <source>Most Significant Bit</source>
        <translation type="obsolete">最前位元</translation>
    </message>
    <message>
        <source>Less Significant Bit</source>
        <translation type="obsolete">最後位元</translation>
    </message>
    <message>
        <source>Premultiplied</source>
        <translation type="obsolete">預乘</translation>
    </message>
    <message>
        <source>Color Map</source>
        <translation type="obsolete">色表</translation>
    </message>
    <message>
        <source>8-bit indexes into a colormap.</source>
        <translation type="obsolete">8位元為索引的色表。</translation>
    </message>
</context>
<context>
    <name>ImgInfoModel</name>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="43"/>
        <source>File Name:</source>
        <translation>檔案名稱：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="45"/>
        <source>File Path:</source>
        <translation>檔案路徑：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="47"/>
        <source>Image Type:</source>
        <translation>圖片類型：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="49"/>
        <source>Size:</source>
        <translation>大小：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="51"/>
        <source>Date Created:</source>
        <translation>建立日期：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="53"/>
        <source>Date Modified:</source>
        <translation>修改日期：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="55"/>
        <source>Animation:</source>
        <translation>動畫：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="57"/>
        <source>Comment:</source>
        <translation>註解：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="68"/>
        <source>Not Supported Format or Broken File</source>
        <translation>不支援的格式或損毀的檔案</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="71"/>
        <source>%1 FILE</source>
        <translation>%1檔</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="82"/>
        <location filename="../../src/imginfomodel.cpp" line="96"/>
        <location filename="../../src/imginfomodel.cpp" line="104"/>
        <source>Not Supported</source>
        <translation>不支援</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="91"/>
        <source>Frame Number: %1, Frame Delay: %2</source>
        <translation>影格數：%1，影格延遲：%2</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="125"/>
        <source>Property</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="127"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="273"/>
        <source>Premultiplied</source>
        <translation>預乘</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="277"/>
        <source>MSB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="279"/>
        <source>LSB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="281"/>
        <source>Color Map</source>
        <translation>色表</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="283"/>
        <source>nRGB - 8888</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="285"/>
        <location filename="../../src/imginfomodel.cpp" line="287"/>
        <source>ARGB - 8888</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="289"/>
        <source>RGB - 565</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="291"/>
        <source>ARGB - 8565</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="293"/>
        <source>nRGB - 6666</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="295"/>
        <source>ARGB - 6666</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="297"/>
        <source>nRGB - 1555</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="299"/>
        <source>AnRGB - 81555</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="301"/>
        <source>RGB - 888</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="303"/>
        <source>nRGB - 4444</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="305"/>
        <source>ARGB - 4444</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="319"/>
        <source>8-bit indexes into a colormap.</source>
        <translation>8位元為索引的色表。</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="355"/>
        <source>Width X Height X Depth
%1-bit per pixel.</source>
        <translation>寬度X高度X深度
每像素%1位元。</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="359"/>
        <source>
Using special algorithm to accelerate the loading speed.
May loss some image quality.</source>
        <translation>
使用特殊演算法加速讀取速度。
可能會損傷圖型品質。</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="363"/>
        <source>
Data of each pixel stored as following: </source>
        <translation>
每像素的資料儲存如下：</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="365"/>
        <source>
	Alpha channel: %1-bit</source>
        <translation>
	透明通道：%1位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="368"/>
        <source>
	nonsense: %1-bit</source>
        <translation>
	無意義：%1位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="371"/>
        <source>
	Red: %1-bit</source>
        <translation>
	紅色：%1位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="374"/>
        <source>
	Green: %1-bit</source>
        <translation>
	綠色：%1位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="377"/>
        <source>
	Blue: %1-bit</source>
        <translation>
	藍色: %1位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="381"/>
        <source>
Bytes are packed with the &quot;%1&quot; first.</source>
        <translation>
位元以&quot;%1&quot;優先的方式存儲。</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="382"/>
        <source>Most Significant Bit</source>
        <translation>最前位元</translation>
    </message>
    <message>
        <location filename="../../src/imginfomodel.cpp" line="383"/>
        <source>Less Significant Bit</source>
        <translation>最後位元</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/mainwindow.ui" line="17"/>
        <location filename="../../src/mainwindow.cpp" line="36"/>
        <location filename="../../src/mainwindow.cpp" line="150"/>
        <source>Li&apos;s Image Viewer</source>
        <translation>李氏圖片檢視器</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="56"/>
        <source>&amp;File</source>
        <translation>檔案(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="70"/>
        <source>&amp;Action</source>
        <translation>動作(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="84"/>
        <source>A&amp;bout</source>
        <translation>關於(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="91"/>
        <source>&amp;Setting</source>
        <translation>設定(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="95"/>
        <source>&amp;Language</source>
        <translation>語言(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="130"/>
        <source>Print</source>
        <translation>列印</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="133"/>
        <source>Ctrl+P</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="141"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="144"/>
        <source>Ctrl++</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="152"/>
        <source>Zoom Out</source>
        <translation>縮小</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="155"/>
        <source>Ctrl+-</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="163"/>
        <source>Normal Size</source>
        <translation>正常大小</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="166"/>
        <source>Ctrl+0</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="180"/>
        <source>Fit to Window</source>
        <translation>調整成視窗大小</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="183"/>
        <source>Ctrl+Alt+0</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="191"/>
        <source>Next Image</source>
        <translation>下一張</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="194"/>
        <source>Right</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="202"/>
        <source>Previous Image</source>
        <translation>前一張</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="205"/>
        <source>Left</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="210"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="213"/>
        <source>Ctrl+O</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="221"/>
        <source>Delete</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="224"/>
        <source>Del</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="232"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="235"/>
        <source>Ctrl+I</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="243"/>
        <location filename="../../src/mainwindow.cpp" line="522"/>
        <source>Duplicate</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="246"/>
        <source>Ctrl+Alt+C</source>
        <extracomment>Don&apos;t translate if you want to keep this shortcut</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="255"/>
        <source>This Program</source>
        <translation>本程式</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="264"/>
        <source>Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="272"/>
        <source>Convert to...</source>
        <translation>轉換為...</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="275"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="286"/>
        <source>&amp;Smart Fit to Window</source>
        <translation>智慧型調為視窗大小(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="294"/>
        <source>Auto Detect</source>
        <translation>自動偵測</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="302"/>
        <source>Program Default</source>
        <translation>程式預設</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="310"/>
        <source>Rotate Counterclockwise</source>
        <translation>左轉90度</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="313"/>
        <source>Ctrl+Left</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="321"/>
        <source>Rotate clockwise</source>
        <translation>右轉90度</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="324"/>
        <source>Ctrl+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="335"/>
        <source>Auto Reset Scale</source>
        <translation>自動重設縮放</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="346"/>
        <source>Auto Reset Rotate</source>
        <translation>自動重設旋轉</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="437"/>
        <source>Open File</source>
        <translation>開啟檔案</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="449"/>
        <source>Convert Image</source>
        <translation>轉換圖片</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="461"/>
        <source>Convert Image failure!</source>
        <translation>轉換圖片失敗！</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="462"/>
        <source>Cannot convert image in &quot;%1&quot; as &quot;%2&quot; file.</source>
        <translation>無法將轉換後&quot;%2&quot;檔儲存於&quot;%1&quot;。</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="508"/>
        <source>Cannot Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="509"/>
        <source>Delete function is not supported on this OS.
Sorry for the inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="540"/>
        <source>Duplicate failure!</source>
        <translation>複製失敗！</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="541"/>
        <source>Cannot Duplicate image in &quot;%1&quot;</source>
        <translation>無法將圖片複製到&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="553"/>
        <source>Cannot Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="554"/>
        <source>This function hasn&apos;t implanted yet.
Sorry for the inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="632"/>
        <source>About Li&apos;s Image Viewer</source>
        <translation>關於李氏圖片檢視器</translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="633"/>
        <source>&lt;p&gt;The &lt;b&gt;Li&apos;s Image Viewer&lt;/b&gt; is designed for better image viewing experience.&lt;br&gt;Use it freely if you like it!&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;Author: Li, Kwue-Ron&lt;br&gt;Email: likwueron@gmail.com&lt;br&gt;Version: %1&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;李氏圖片檢視器&lt;/b&gt;是為了更好的賞圖體驗而設計的。&lt;br&gt;請隨意利用它！&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;作者：Li, Kwue-Ron&lt;br&gt;Email：likwueron@gmail.com&lt;br&gt;版本： %1&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PropertyDialog</name>
    <message>
        <source>Property</source>
        <translation type="obsolete">屬性</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">值</translation>
    </message>
    <message>
        <source>File Name:</source>
        <translation type="obsolete">檔案名稱：</translation>
    </message>
    <message>
        <source>File Path:</source>
        <translation type="obsolete">檔案路徑：</translation>
    </message>
    <message>
        <source>Image Type:</source>
        <translation type="obsolete">圖片類型：</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="obsolete">大小：</translation>
    </message>
    <message>
        <source>Show in width, height and depth, respectively.</source>
        <translation type="obsolete">依序顯示寬度、高度和深度。</translation>
    </message>
    <message>
        <source>Date Created:</source>
        <translation type="obsolete">建立日期：</translation>
    </message>
    <message>
        <source>Date Modified:</source>
        <translation type="obsolete">修改日期：</translation>
    </message>
    <message>
        <source>Animation:</source>
        <translation type="obsolete">動畫：</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation type="obsolete">註解：</translation>
    </message>
    <message>
        <location filename="../../src/propertydialog.ui" line="30"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../../src/propertydialog.cpp" line="44"/>
        <source>Properties of %1</source>
        <translation>%1的屬性</translation>
    </message>
</context>
</TS>
